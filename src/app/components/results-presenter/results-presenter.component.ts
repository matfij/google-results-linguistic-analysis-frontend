import { Component, Input } from '@angular/core';
import { AnalysisResult } from 'src/app/data/models';

@Component({
  selector: 'app-results-presenter',
  templateUrl: './results-presenter.component.html',
  styleUrls: ['./results-presenter.component.scss']
})
export class ResultsPresenterComponent {
  @Input() analysisResults: AnalysisResult[];

  constructor() {}

  onClearAnalysisResult(ind: number) {
    this.analysisResults.splice(ind, 1);
  }

}
