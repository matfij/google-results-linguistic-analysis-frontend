import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { AnalysisParams, AnalysisResult, AppConstant, ErrorMessage, SearchResult } from 'src/app/data/models';
import { ApiService } from 'src/app/services/api.service';
import { ConstantsService } from 'src/app/services/constants.service';

@Component({
  selector: 'app-analyze-form',
  templateUrl: './analyze-form.component.html',
  styleUrls: ['./analyze-form.component.scss']
})
export class AnalyzeFormComponent implements OnInit {
  @Input() searchResults: SearchResult[];
  @Output() analysisResults = new EventEmitter<AnalysisResult[]>();
  @Output() clearSearchResult = new EventEmitter<boolean>();
  _selectablePages: AppConstant[];
  _selectedPages: AppConstant[] = [];
  _selectableAnalysisTypes: AppConstant[];
  _selectedAnalysisTypes: AppConstant[] = [];
  _dropdownSettings: IDropdownSettings;
  _errorMessage: string;
  _loading: boolean;

  constructor(
    private _apiService: ApiService,
    private _constantsService: ConstantsService,
    private _translateService: TranslateService
  ) {
    this._errorMessage = null;
  }

  ngOnInit() {
    this._dropdownSettings = {
      idField: 'value',
      textField: 'name',
      selectAllText: this._translateService.instant('forms.selectAll'),
      unSelectAllText: this._translateService.instant('forms.unselectAll'),
      itemsShowLimit: 3
    };

    this._selectableAnalysisTypes = this._constantsService.ANALYSIS_TYPE;

    this._selectablePages = this.searchResults.map((page, ind) => {
      return {
        value: ind,
        name: page.title.substr(0, 50) + '...'
      };
    });
  }

  analyze() {
    this._errorMessage = null;
    const params: AnalysisParams = {
      analyzed_pages: this._selectedPages.map(page => { return page.value }),
      analysis_types: this._selectedAnalysisTypes.map(type => { return type.value })
    };
    this._loading = true;
    this._apiService.analyze(params).subscribe((result: AnalysisResult[]) => {
      this.analysisResults.emit(result);
      this._loading = false;
    }, (error: ErrorMessage) => {
      this._loading = false;
      this._errorMessage = error.error;
    });
  }

  onClearSearchResult() {
    this.searchResults = null;
    this.clearSearchResult.next(true);
  }
}
