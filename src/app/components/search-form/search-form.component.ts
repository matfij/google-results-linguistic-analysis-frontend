import { Component, Output, EventEmitter } from '@angular/core';
import { ErrorMessage, SearchParams, SearchResult } from 'src/app/data/models';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent {
  _pageNumber: number;
  _searchPhrase: string;
  _errorMessage: string;
  _loading: boolean;
  @Output() resultPages = new EventEmitter<SearchResult[]>();

  constructor(
    private _apiService: ApiService
  ) {
    this._errorMessage = null;
  }

  search(pageNumber: number, searchPhrase: string) {
    this._errorMessage = null;
    const params: SearchParams = {
      page_number: +pageNumber,
      search_phrase: searchPhrase
    };
    this._loading = true;
    this._apiService.search(params).subscribe((result: SearchResult[]) => {
      this.resultPages.emit(result);
      this._loading = false;
    }, (error: ErrorMessage) => {
      this._loading = false;
      this._errorMessage = error.error;
    });
  }

}
