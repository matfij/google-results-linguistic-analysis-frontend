import { Component, OnInit } from '@angular/core';
import { AnalysisResult, SearchResult } from './data/models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  resultPages: SearchResult[];
  analysisResults: AnalysisResult[];

  ngOnInit() {
    this.resultPages = null;
    this.analysisResults = null;
  }

  onGetResultPages(pages: SearchResult[]) {
    this.resultPages = pages;
  }

  onGetAnalysisResults(results: AnalysisResult[]) {
    this.analysisResults = results;
  }

  onClearSearchResult() {
    this.resultPages = null;
    this.analysisResults = null;
  }

}
