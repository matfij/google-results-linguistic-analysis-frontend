export enum AnalysisType {
  PageInfo,
  CommonWords,
  References,
  SentencesLength,
  GraphicalContent,
  HyperReferences
}
