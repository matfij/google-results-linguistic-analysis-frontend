import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  constructor(
    // private toastController: ToastController
  ) {}

  async presentToast(message: string, duration: number = 2000) {
    // const toast = await this.toastController.create({
    //   message: message,
    //   duration: duration
    // });
    // toast.present();
  }

  getDate(): string {
    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/" + (currentdate.getMonth()+1) + "/" + currentdate.getFullYear() + " "
      + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

    return datetime;
  }

}
