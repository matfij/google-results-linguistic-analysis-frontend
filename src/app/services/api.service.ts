import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AnalysisParams, AnalysisResult, SearchResult, SearchParams } from '../data/models';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _BASE_URL = environment.apiUrl;

  constructor(
    private _httpClient: HttpClient
  ) {}

  search(params: SearchParams): Observable<SearchResult[]> {
    const endpoint = '/search';
    return this._httpClient.post<SearchResult[]>(this._BASE_URL + endpoint, params);
  }

  analyze(params: AnalysisParams): Observable<AnalysisResult[]> {
    const endpoint = '/analyze';
    return this._httpClient.post<AnalysisResult[]>(this._BASE_URL + endpoint, params);
  }
}
