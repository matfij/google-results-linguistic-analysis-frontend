import { AnalysisType } from './enums';


export interface AppConstant {
  value: number;
  name: string;
}

export interface ErrorMessage {
  name: string;
  error: string;
  status: number;
}

export interface SearchParams {
  page_number: number;
  search_phrase: string;
}

export interface SearchResult {
  title: string;
}

export interface AnalysisParams {
  analyzed_pages: number[];
  analysis_types: number[];
}

export interface AnalysisResult {
  analysis_type: AnalysisType;
  images?: string[];
  result_rows?: string[];
}
