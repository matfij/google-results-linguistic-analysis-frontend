import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AnalysisType } from '../data/enums';
import { AppConstant } from '../data/models';


@Injectable({
  providedIn: 'root'
})
export class ConstantsService {
  public readonly ANALYSIS_TYPE = this.enumToConst(AnalysisType, {namePrepend: 'analysiType.'});

  constructor(
    private _translteService: TranslateService
  ) {}

  public enumToConst(enumObj, options?: { namePrepend?: string, valueProperty?: string, nameProperty?: string }): AppConstant[] {
    const defaultOptions = { namePrepend: '', valueProperty: 'value', nameProperty: 'name'};
    options = { ...defaultOptions, ...options };

    return Object.keys(enumObj).filter(item => isNaN(Number(item))).map(item => {
      return {
        value: enumObj[item],
        name: this._translteService.instant(options.namePrepend + item)
      };
    });
  }
}
